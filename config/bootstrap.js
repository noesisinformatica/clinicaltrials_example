/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */

var nodepath = require('path'),
    seedInfo;

function hasSeedInfo() {
    var seedPath = nodepath.resolve(sails.config.paths.config, 'env', sails.config.environment, 'seed');
    try {
        seedInfo = require(seedPath);
        sails.log.debug('Seed Info : ', seedInfo);
    } catch (e) {}
    return !_.isUndefined(seedInfo) && !_.isNull(seedInfo);
}

function doSeed(callback) {
    var self = this;
    // sails.log.debug('self : ', self);
    _.each(seedInfo, function(value, key) {
        var model = self[key];
        // sails.log.debug('value : ', value);
        // sails.log.debug('key : ', key);
        // sails.log.debug('model : ', model);
        if (!_.isNull(model) && !_.isUndefined(model)) {
            model.count().exec(function(err, count) {
                if (!err && count === 0 && !_.isEmpty(value)) {
                    sails.log.debug('Seeding ' + key + '...');
                    model.createEach(value).exec(function(err, results) {
                        if (err) {
                            sails.log.warn('Error seeding model['+key+'] :' , err);
                        } else {
                            sails.log.debug((key + ' seed planted').grey);
                        }
                    });
                } else {
                    sails.log.debug((key + ' had models, so no seed needed').grey);
                }
            });
        }
    });
    callback();
}

module.exports.bootstrap = function(cb) {

	sails.log.info('sails.config.seed : ', sails.config.seed); 
    // It's very important to trigger this callback method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
    if (sails.config.seed.load && hasSeedInfo()) {
        async.series([doSeed], cb);
    } else {
    	  cb();
    }
};
