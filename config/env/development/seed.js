//app/config/env/development/seed.js
module.exports = {

    patient: [
      {
        "PatientID": 42,
        "FirstName": "Andy",
        "LastName": "Stams",
        "Email": "andy@email.com",
        "Telephone": 93675,
        "Age": 23,
        "Gender": 0,
        "AddressLine1": "Address1",
        "AddressLine2": "Add",
        "PostCode": "HA90AU",
        "GP": "grp",
        "Password": "$2a$10$0TnrfKFlcRb5RWDQx4M4LeFljphPsTDc1vAeGQKnKt50glV8jYjhu"
      },
      {
        "PatientID": 45,
        "FirstName": "Dol",
        "LastName": "Dod",
        "Email": "dol@test.com",
        "Telephone": 9,
        "Age": 23,
        "Gender": 0,
        "AddressLine1": "ad",
        "AddressLine2": "ad",
        "PostCode": "pc",
        "GP": "gp",
        "Password": "$2a$10$82aicR1WEckfiXnYeZvycej0VYqiw5IkJF9mx.dCB.lcvoOcG9JqK"
      },
      {
        "PatientID": 46,
        "FirstName": "gaga",
        "LastName": "lady",
        "Email": "gaga@email.com",
        "Telephone": 0,
        "Age": 20,
        "Gender": 1,
        "AddressLine1": "Room 1",
        "AddressLine2": "UCL St",
        "PostCode": "W1T6HD",
        "GP": "JIM",
        "Password": "$2a$10$I110820Ki9NA.yAAi/riNe8.g9bGJzYLXYuFbY9I50jAgHDLtE/IW"
      },
      {
        "PatientID": 47,
        "FirstName": "Andy",
        "LastName": "Stamdfjha",
        "Email": "andy@email.com",
        "Telephone": 93675,
        "Age": 1,
        "Gender": 0,
        "AddressLine1": "ad",
        "AddressLine2": "ad",
        "PostCode": "pc",
        "GP": "grp",
        "Password": "$2a$10$GSQ0JqqI6bljQGR0DcmkJu8RBe5lk4OLGrSYDoo0Qv4Bw8XX9MvfW"
      },
      {
        "PatientID": 48,
        "FirstName": "hey",
        "LastName": "j",
        "Email": "hey@email.com",
        "Telephone": 1234567,
        "Age": 21,
        "Gender": 1,
        "AddressLine1": "flat1",
        "AddressLine2": "UCL St",
        "PostCode": 123,
        "GP": "someone",
        "Password": "$2a$10$Ta4gxyY3fxLgV0k35XJrx.gguHfzXDBA3e0evKhbqwXn9tzx/ld8C"
      },
      {
        "PatientID": 49,
        "FirstName": "hello",
        "LastName": "hey",
        "Email": "hello@email.com",
        "Telephone": 12333,
        "Age": 12,
        "Gender": 1,
        "AddressLine1": 12,
        "AddressLine2": 1222,
        "PostCode": 123,
        "GP": 12222,
        "Password": "$2a$10$bG83OiG722gjR5KlyHnIDegz5tKK./OOnnizUhFpRYhHK0NhSP1We"
      }
    ],
    Condition: [
  {
    "id": 1,
    "name": "Condition",
    //"Code": 11111111,
    //"Description": "A bad condition",
    //"SnomedID": 11111111,
    "state": "State",
    "conditionAge": "2 months"
  },
  {
    "id": 2,
    "name": "Another",
    //"Code": 2222222,
    //"Description": "A worse condition",
    //"SnomedID": 2222222,
    "state": "Statez",
    "conditionAge": "1 month"
  }
],
    clinicalcentre: [
  {
    "id": 1,
    "name": "Centre",
    "addressLine1": "Address1",
    "addressLine2": "Address2",
    "postCode": "PCode",
    "openingTimes": "14:00-18:00"
  },
  {
    "id": 2,
    "name": "Cent2",
    "addressLine1": "Olypmou 14",
    "addressLine2": "Engomi",
    "postCode": 2411,
    "openingTimes": "09:00-17:00"
  },
  {
    "id": 3,
    "name": "Cent3",
    "addressLine1": "North Road",
    "addressLine2": "London",
    "postCode": "Tw200ee",
    "openingTimes": "09:00-17:00"
  }
],

    HealthProfessional: [
  {
    "FirstName": "June",
    "LastName": "Ellis",
    "Email": "june@test.com",
    "Telephone": 7,
    "Age": 36,
    "Gender": 0,
    "AddressLine1": "ad",
    "AddressLine2": "ad",
    "PostCode": "po",
    "GMCNumber": 123,
    "Role": "role",
    "WorksAtCenters": 1,
    "ChiefFlag": 0,
    "PrincipalFlag": 1,
    "ChiefOfTrials": 1,
    "PrincipalOfCenter": 1,
    "Password": "pass",
    "ProfessionalID": 1
  },
  {
    "FirstName": "Lynda",
    "LastName": "comlol",
    "Email": "lynda@email.com",
    "Telephone": 8,
    "Age": 42,
    "Gender": 0,
    "AddressLine1": "Add",
    "AddressLine2": "Add",
    "PostCode": "PO",
    "GMCNumber": 123,
    "Role": "Role",
    "WorksAtCenters": 1,
    "ChiefFlag": 0,
    "PrincipalFlag": 1,
    "ChiefOfTrials": 0,
    "PrincipalOfCenter": 0,
    "Password": "$2a$10$UQwheEiktdQTlhRbWG6zbutjUP6E6mXnC4mjRKxe40YVkpGBcz3/G",
    "ProfessionalID": 2
  }
],

    Manager: [
  {
    "ManagerID": 1,
    "FirstName": "Manny",
    "LastName": "Man",
    "Email": "manager@email.com",
    "Telephone": 9,
    "Age": 50,
    "Gender": 1,
    "AddressLine1": "Add",
    "AddressLine2": "add",
    "PostCode": "ha",
    "Password": "$2a$10$UQwheEiktdQTlhRbWG6zbutjUP6E6mXnC4mjRKxe40YVkpGBcz3/G"
  }
],
    Treatment: [
  {
    "name": 1,
    "code": 8888888,
    "description": "abc",
    "id": 1
  },
  {
    "name": 1,
    "code": 7777777,
    "description": "abc",
    "id": 2
  }
],
    Trial_Centre: [
  {
    "CentreID": 3,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 1,
    "TrialID": 86,
    "EndDate": "2016-10-08"
  },
  {
    "CentreID": 2,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 1,
    "TrialID": 86,
    "EndDate": "2016-09-08"
  },
  {
    "CentreID": 2,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 2,
    "TrialID": 87,
    "EndDate": "2016-10-08"
  },
  {
    "CentreID": 1,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 1,
    "TrialID": 88,
    "EndDate": "2016-09-08"
  },
  {
    "CentreID": 3,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 1,
    "TrialID": 87,
    "EndDate": "2016-09-08"
  },
  {
    "CentreID": 1,
    "StartDate": "2016-08-08",
    "PrincipalInvestigator": 1,
    "TrialID": 87,
    "EndDate": "2016-09-08"
  }
],

   Trial: [
  {
    "TrialID": 86,
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "AbbreviationName": "OffN",
    "KeyWords": "cancer",
    "BriefSummary": "lung",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "DetailedDescription": "Detailed description for trial 86"
  },
  {
    "TrialID": 87,
    "ExternalTrialID": 123,
    "OfficialName": "Official Name",
    "AbbreviationName": "ON",
    "KeyWords": "cancer",
    "BriefSummary": "lung, brain",
    "ChiefInvestigator": 2,
    "TrialStatus": 1,
    "DetailedDescription": "Detailed description for trial 87"
  },
  {
    "TrialID": 88,
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "AbbreviationName": "ON",
    "KeyWords": "cancer",
    "BriefSummary": "kidney, prostate",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "DetailedDescription": "Detailed description for trial 88"
  }
],
    Trial_Inclusion: [
      {
        "Trial_Inc_ID": 26,
        "TrialID": 86,
        "InclusionID": 42,
        "Date": "2016-08-10"
      },
      {
        "Trial_Inc_ID": 27,
        "TrialID": 87,
        "InclusionID": 43,
        "Date": "2016-08-10"
      },
      {
        "Trial_Inc_ID": 28,
        "TrialID": 88,
        "InclusionID": 44,
        "Date": "2016-08-10"
      },
      {
        "Trial_Inc_ID": 29,
        "TrialID": 86,
        "InclusionID": 45,
        "Date": "2016-08-13"
      },
      {
        "Trial_Inc_ID": 30,
        "TrialID": 86,
        "InclusionID": 46,
        "Date": "2016-08-13"
      },
      {
        "Trial_Inc_ID": 31,
        "TrialID": 86,
        "InclusionID": 47,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 32,
        "TrialID": 86,
        "InclusionID": 48,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 33,
        "TrialID": 86,
        "InclusionID": 49,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 34,
        "TrialID": 86,
        "InclusionID": 50,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 35,
        "TrialID": 86,
        "InclusionID": 51,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 36,
        "TrialID": 86,
        "InclusionID": 52,
        "Date": "2016-08-15"
      },
      {
        "Trial_Inc_ID": 37,
        "TrialID": 86,
        "InclusionID": 53,
        "Date": "2016-08-15"
      }
    ],
    Trial_Exclusion : [
  {
    "TrialID": 86,
    "ExclusionID": 51,
    "Trial_Exc_ID": 39,
    "Date": "2016-08-10"
  },
  {
    "TrialID": 87,
    "ExclusionID": 52,
    "Trial_Exc_ID": 40,
    "Date": "2016-08-10"
  },
  {
    "TrialID": 88,
    "ExclusionID": 53,
    "Trial_Exc_ID": 41,
    "Date": "2016-08-10"
  },
  {
    "TrialID": 86,
    "ExclusionID": 57,
    "Trial_Exc_ID": 45,
    "Date": "2016-08-13"
  },
  {
    "TrialID": 86,
    "ExclusionID": 58,
    "Trial_Exc_ID": 46,
    "Date": "2016-08-13"
  },
  {
    "TrialID": 86,
    "ExclusionID": 59,
    "Trial_Exc_ID": 47,
    "Date": "2016-08-13"
  },
  {
    "TrialID": 86,
    "ExclusionID": 60,
    "Trial_Exc_ID": 48,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 86,
    "ExclusionID": 61,
    "Trial_Exc_ID": 49,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 86,
    "ExclusionID": 62,
    "Trial_Exc_ID": 50,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 86,
    "ExclusionID": 63,
    "Trial_Exc_ID": 51,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 86,
    "ExclusionID": 64,
    "Trial_Exc_ID": 52,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 86,
    "ExclusionID": 65,
    "Trial_Exc_ID": 53,
    "Date": "2016-08-15"
  },
  {
    "TrialID": 87,
    "ExclusionID": 66,
    "Trial_Exc_ID": 54,
    "Date": "2016-08-15"
  }
],

    Trial_Edit: [
  {
    "ManagerID": 1,
    "TrialID": 1,
    "Date": "2016-08-03",
    "AbbreviationName": "New Title",
    "BriefSummary": "Previous Description",
    "DetailedDescription": "New Description",
    "ExternalTrialID": 1,
    "OfficialName": 1,
    "EditTime": "09:30:00.0000000",
    "ChiefInvestigator": "",
    "TrialStatus": "",
    "KeyWords": ""
  },
  {
    "ManagerID": 1,
    "TrialID": 86,
    "Date": "2016-08-13",
    "AbbreviationName": "ON",
    "BriefSummary": "Brief Summary old",
    "DetailedDescription": "Detailed description",
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "EditTime": "19:16:12.0000000",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "KeyWords": "cancer"
  },
  {
    "ManagerID": 1,
    "TrialID": 86,
    "Date": "2016-08-13",
    "AbbreviationName": "ON",
    "BriefSummary": "Brief Summary test",
    "DetailedDescription": "Detailed description",
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "EditTime": "19:16:43.0000000",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "KeyWords": "cancer"
  },
  {
    "ManagerID": 1,
    "TrialID": 86,
    "Date": "2016-08-15",
    "AbbreviationName": "OffN",
    "BriefSummary": "Brief Summary test",
    "DetailedDescription": "Detailed description",
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "EditTime": "17:48:15.0000000",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "KeyWords": "cancer"
  },
  {
    "ManagerID": 1,
    "TrialID": 86,
    "Date": "2016-08-15",
    "AbbreviationName": "Off",
    "BriefSummary": "Brief Summary test",
    "DetailedDescription": "Detailed description",
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "EditTime": "18:24:56.0000000",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "KeyWords": "cancer"
  },
  {
    "ManagerID": 1,
    "TrialID": 86,
    "Date": "2016-08-15",
    "AbbreviationName": "OffN",
    "BriefSummary": "Brief Summary test",
    "DetailedDescription": "Detailed description",
    "ExternalTrialID": 12,
    "OfficialName": "Official Name",
    "EditTime": "18:25:06.0000000",
    "ChiefInvestigator": 1,
    "TrialStatus": 1,
    "KeyWords": "cancer"
  }
],
    Trial_Delete: [
  {
    "ManagerID": 1,
    "TrialID": 1,
    "Date": "2016-08-03",
    "DeletionTimestamp": new Date()
  }
],
    Trial_Condition: [
  {
    "TrialID": 86,
    "ConditionID": 1
  },
  {
    "TrialID": 87,
    "ConditionID": 1
  },
  {
    "TrialID": 88,
    "ConditionID": 1
  }
],
    Trial_Add: [
  {
    "TrialID": 1,
    "Date": "2016-08-10",
    "Time": new Date(),
    "ManagerID": 1
  },
  {
    "TrialID": 88,
    "Date": "2016-08-10",
    "Time": new Date(),
    "ManagerID": 1
  }
],
    Patient_Treatment: [
  {
    "PatientID": 42,
    "TreatmentID": 1,
    "DateDiagnosed": "2016-08-03",
    "DateEnded": "",
    "Response": "Response"
  }
],
    Patient_Condition: [
  {
    "PatientID": 42,
    "ConditionID": 1,
    "DateDiagnosed": "2016-08-04",
    "DateEnded": "",
    "State": "State"
  }
],

    InclusionCriteria: [
  {
    "TrialID": 86,
    "InclusionID": 42,
    "Gender": 1,
    "MinimumAge": 18,
    "MaximumAge": 65
  },
  {
    "TrialID": 87,
    "InclusionID": 43,
    "Gender": 1,
    "MinimumAge": 18,
    "MaximumAge": 65
  },
  {
    "TrialID": 88,
    "InclusionID": 44,
    "Gender": 0,
    "MinimumAge": 18,
    "MaximumAge": 65
  },
  {
    "TrialID": 86,
    "InclusionID": 45,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 65
  },
  {
    "TrialID": 86,
    "InclusionID": 46,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 64
  },
  {
    "TrialID": 86,
    "InclusionID": 47,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 63
  },
  {
    "TrialID": 86,
    "InclusionID": 48,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 64
  },
  {
    "TrialID": 86,
    "InclusionID": 49,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 63
  },
  {
    "TrialID": 86,
    "InclusionID": 50,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 63
  },
  {
    "TrialID": 86,
    "InclusionID": 51,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 64
  },
  {
    "TrialID": 86,
    "InclusionID": 52,
    "Gender": 1,
    "MinimumAge": 19,
    "MaximumAge": 63
  },
  {
    "TrialID": 86,
    "InclusionID": 53,
    "Gender": 1,
    "MinimumAge": 18,
    "MaximumAge": 63
  }
],
    ExclusionCriteria: [
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 16,
    "MaximumAge": 66,
    "ExclusionID": 51
  },
  {
    "TrialID": 87,
    "Gender": 1,
    "MinimumAge": 16,
    "MaximumAge": 68,
    "ExclusionID": 52
  },
  {
    "TrialID": 88,
    "Gender": 1,
    "MinimumAge": 17,
    "MaximumAge": 66,
    "ExclusionID": 53
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 16,
    "MaximumAge": 67,
    "ExclusionID": 57
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 16,
    "MaximumAge": 68,
    "ExclusionID": 58
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 16,
    "MaximumAge": 65,
    "ExclusionID": 59
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 15,
    "MaximumAge": 6,
    "ExclusionID": 60
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 15,
    "MaximumAge": 65,
    "ExclusionID": 61
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 15,
    "MaximumAge": 65,
    "ExclusionID": 62
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 15,
    "MaximumAge": 66,
    "ExclusionID": 63
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 14,
    "MaximumAge": 66,
    "ExclusionID": 64
  },
  {
    "TrialID": 86,
    "Gender": 0,
    "MinimumAge": 15,
    "MaximumAge": 66,
    "ExclusionID": 65
  },
  {
    "TrialID": 87,
    "Gender": 1,
    "MinimumAge": 16,
    "MaximumAge": 69,
    "ExclusionID": 66
  }
]

};
