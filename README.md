# Clinical Trials Example Application

This is a [Sails](http://sailsjs.org) application that serves as a registry for clinical trials. It uses [Termlex](https://termlex.gitlab.io) 
as the terminology server to use SNOMED CT codes as conditions and treatments included in the trial protocols.

This project was forked and adapted from the original at [https://github.com/Asta1701/clinicaltrialsWA](https://github.com/Asta1701/clinicaltrialsWA)

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools (like
[Bower][] and [BrowserSync][]). You will only need to run this command when dependencies change in package.json.

    npm install

## Running the example

Run the application using the following command.

    sails lift
    
## Release updates

If you want to make changes and create updated version, then use sg_release (https://github.com/SunGard-Labs/grunt-sg-release) that is part of this project

```
// test changes that will be made before committing any changes
grunt bump --dry-run
// perform release -- increment version and commit changes using gitflow model
grunt release
```

Happy Hacking!
