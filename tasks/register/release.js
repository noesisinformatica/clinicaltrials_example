/**
 * `release`
 *
 */
module.exports = function(grunt) {
  grunt.registerTask('release', [
    'clean',
    'conventionalChangelog',
    'sg_release:doRelease'
  ]);
};
