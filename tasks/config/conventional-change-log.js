/**
 * `convention-changelog`
 *
 */
module.exports = function(grunt) {

  grunt.config.set('conventionalChangelog', {
    options: {
      changelogOpts: {
        preset: 'angular'
      }
      // host and repo settings inherited from package.json so don't need to specify context option
    },
    release: {
      src: 'CHANGELOG.md'
    }
  });

  grunt.loadNpmTasks('grunt-conventional-changelog');

};
