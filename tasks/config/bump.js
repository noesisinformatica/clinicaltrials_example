/**
 * `convention-changelog`
 *
 */
module.exports = function(grunt) {

  grunt.config.set('bump', {
    scripts: {
      files: ["*.json, app/js/*.js", "test/*.js"],
              updateConfigs: ["pkg"],
              commitFiles: ["-a"],
              push: false
    }
  });

  grunt.loadNpmTasks('grunt-bump');

};
