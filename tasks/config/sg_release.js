/**
 * `sg-release`
 *  See https://github.com/SunGard-Labs/grunt-sg-release
 */
module.exports = function(grunt) {

    grunt.config.set('sg_release', {
        doRelease: {
            options: {
                skipBowerInstall: true,
                        developBranch: 'develop',
                        masterBranch: 'master',
                        files: [
                    'package.json'
                ],
                push: false, // push during the first bump phase is deactivated by default
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['-a'], // '-a' for all files
                pushTo: 'origin',
                mergeOptions: ''
            }
        }
    });

    grunt.loadNpmTasks('grunt-sg-release');

};
