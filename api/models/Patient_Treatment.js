module.exports = {
    
    schema: true,
    
    attributes:{
        PatientID:{
            type:'integer',
            required: false,
        },
        TreatmentID:{
            type: 'integer',
            required: true
        },
        DateDiagnosed:{
            type: 'Date',
            required: true
        },
        DateEnded: {
            type: 'date',
            required: false // patient can have on-going treatment
        },
        Response: {
            type:'string',
            required: false
        }

    }
}