/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {
    
    schema: true,
    
    attributes:{
        Trial_Inc_ID:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        TrialID:{
            type: 'integer',
            required: true
        },
        InclusionID:{
            type: 'integer',
            required: true
        },
        Date: {
            type: 'date',
            required: false,
            defaultsTo: new Date()
        },
        Time: {
            type:'date',
            required: false,
            defaultsTo: new Date()
        }

    }
}