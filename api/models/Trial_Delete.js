/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {
    
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,

        },
        ManagerID:{
            type: 'integer',
            required: true
        },
        Date:{
            type: 'date',
            required: true,
            defaultsTo: new Date()
        },
        DeletionTimestamp: {
            type: 'date',
            required: true
        }
    }
}