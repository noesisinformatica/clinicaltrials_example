module.exports = {
    
    schema: true,
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,

        },
        ConditionID:{
            type: 'string',
            required: true
        }

    }
}