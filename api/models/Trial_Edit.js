/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {


    schema: true,
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,
        },
        ManagerID:{
            type: 'integer',
            required: true
        },
        Date:{
            type: 'Date',
            required: true
        },
        PreviousTitle: {
            type: 'string',
            required: false
        },
        NewTitle: {
            type:'string',
            required: false
        },
        PreviousDescription: {
            type: 'string',
            required : false
        },
        NewDescription:{
            type: 'string',
            required : false
        },
        EditTimestamp:{
            type: 'date',
            required: false,
            defaultsTo: new Date()
        },
        PreviousExc: {
            type: 'integer',
            required: false
        },
        NewExc: {
            type: 'integer',
            required: false
        },
        PreviousInc :{
            type: 'integer',
            required: false
        },
        NewInc :{
            type: 'integer',
            required: false
        }

    }
}