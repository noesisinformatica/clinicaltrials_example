module.exports = {
    
    schema: true,
    
    attributes:{

        PatientID:{
            type: 'integer',
            required: true
        },
        ConditionID:{
            type: 'integer',
            required: true
        },
        DateDiagnosed: {
            type: 'date',
            required: true
        },
        DateEnded: {
            type:'date',
            required: false // patient can have on-going disease
        },
        State:{
            type:'string',
            required: false
        }

    }
};