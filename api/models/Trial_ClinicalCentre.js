/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {

    schema: true,
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,

        },
        CentreID:{
            type: 'integer',
            required: true
        },
        StartDate:{
            type: 'string',
            required: true
        },
        EndDate: {
            type: 'string',
            required: true
        },
        PrincipalInvestigator: {
            type:'integer',
            required: true
        },
        TrialStatus: {
            type: 'string',
            required : false
        }
    }
}