module.exports = {
    
    schema: true,
    
    attributes:{
        ExclusionID:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        TrialID:{
            type: 'integer',
            required: true
        },
        Gender:{
            type: 'integer',
            required: true
        },
        MinimumAge: {
            type: 'integer',
            required: true
        },
        MaximumAge: {
            type:'integer',
            required: true
        }

    }
}