/**
 * Trial.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {

    schema:true,
    
    attributes:{
        
        TrialID:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        ExternalTrialID:{
            type: 'string',
            required: true
        },
        OfficialName:{
            type: 'String',
            required: true
        },
        AbbreviationName: {
            type: 'string',
            required: true
        },
        KeyWords:{
            type: 'string',
            required : false
        },
        BriefSummary:{
            type: 'string',
            required: true,
        },
        ChiefInvestigator: {
            type: 'integer',
            required: true
        },
        TrialStatus: {
            type: 'integer',
            required: true
        },
        DetailedDescription :{
            type: 'string',
            required: true
        }

    }
}