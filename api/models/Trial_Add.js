/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {
    
    schema: true,
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,
        },
        Date:{
            type: 'Date',
            required: true,
            defaultsTo: new Date()
        },
        Time:{
            type: 'Date',
            required: true,
            defaultsTo: new Date()
        },
        ManagerID: {
            type: 'integer',
            required: true
        }

    }
}