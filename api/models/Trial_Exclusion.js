module.exports = {
    
    schema: true,
    
    attributes:{
        Trial_Exc_ID:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        TrialID:{
            type: 'integer',
            required: true
        },
        ExclusionID:{
            type: 'integer',
            required: true
        },
        Date: {
            type: 'date',
            required: true,
            defaultsTo: new Date()
        },
        Time: {
            type:'date',
            required: false,
            defaultsTo: new Date()
        }

    }
}