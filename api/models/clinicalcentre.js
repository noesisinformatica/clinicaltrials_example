/**
 * Patient.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
    
    schema: true,

    attributes: {
        id:{
            type:'integer',
            required: false
        },
        name:{
            type: 'string',
            required: true
        },
        addressLine1:{
            type: 'string',
            required: true
        },
        addressLine2:{
            type:'string',
            required: false
        },
        postCode:{
            type: 'string',
            required: true
        },
        openingTimes:{
            type:'string',
            required: true
        }
    }
};

