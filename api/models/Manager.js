/**
 * Manager.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    //to ensure you avoid inserting errors caused by the automatic generated attributes such as
    // creation timestamps and id you can use the config below
    //only saves the following to the database as it takes the object and compares it to the schema
    schema: true,

    attributes: {
        ManagerID:{
            type:'integer',
            required: false,
            primaryKey:true
        },
        FirstName:{
            type: 'string',
            required: true
        },
        LastName:{
            type:'string',
            required: true
        },
        Email:{
            type: 'string',
            email:true,
            required: true
        },
        Telephone:{
            type:'integer',
            required: false
        },
        Age:{
            type: 'integer',
            required: true
        },
        Gender:{
            type:'integer',
            required: true
        },
        AddressLine1:{
            type: 'string',
            required: true
        },
        AddressLine2:{
            type:'string',
            required: false
        },
        Password:{
            type:'string',
            required: false
        },
        //avoid displaying password when the client translates patients to json objects automatically
        //also disabled creation timestamps and id as they were interfering with database insertions
        toJSON: function() {
            var obj = this.toObject();
            delete obj.Password;
            return obj;
        }
    },
    beforeCreate: function(values, next){
        // do not do password confirmation validation in model - should do it in UI
        // because model should have no idea that you have a confirmPassword field in UI
        if(!values.Password){
            return next({err: ["Password and password confirmation don't match"]});
        }
        require('bcrypt').hash(values.Password, 10, function passwordEncrypted(err, encryptedPassword) {
            if(err) return next(err);
            values.Password = encryptedPassword;
            //values.online = true;
            next();
        });
    }
};