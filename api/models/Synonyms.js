module.exports = {
    
    schema: true,
    
    attributes:{
        SynonymID:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        FormalName:{
            type: 'string',
            required: true
        },
        Synonym:{
            type: 'string',
            required: true
        }
    }
}