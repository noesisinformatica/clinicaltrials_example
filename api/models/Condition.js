module.exports = {
    
    schema: true,
    
    attributes:{
        id:{
            type:'integer',
            required: false,
            primaryKey:true

        },
        name:{
            type: 'string',
            required: true
        },
        //code:{
        //    type: 'integer',
        //    required: true
        //},
        //description: {
        //    type: 'string',
        //    required: true
        //},
        //snomedID: {
        //    type:'integer',
        //    required: false
        //},
        state: {
            type: 'string',
            required : false
        },
        conditionAge:{
            type: 'string',
            required : false
        }

    }
};