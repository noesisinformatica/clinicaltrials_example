/**
 * Created by Andreas on 04/08/2016.
 */
module.exports = {

    schema: true,
    
    attributes:{
        TrialID:{
            type:'integer',
            required: true,
        },
        TreatmentID:{
            type: 'integer',
            required: true
        },
        TreatmentAge:{
            type: 'String',
            required: false
        }

    }
}