/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');

module.exports = {
    //this action will render/redirect to the form page
    'new': function(req, res ){
        //req.HealthcareProfessionalLogin.authenticated = true;
        //console.log(req.HealthcareProfessionalLogin);
        res.view();
    },
    create: function(req, res, next) {

        // Check for email and password in params sent via the form, if none
        // redirect the browser back to the sign-in form.
        if (!req.param('Email') || !req.param('Password')) {
            // return next({err: ["Password doesn't match password confirmation."]});

            var HealthcareProfessionalnamePasswordRequiredError = [{
                name: 'HealthcareProfessionalnamePasswordRequired',
                message: 'You must enter both a username and password.'
            }]

            // Remember that err is the object being passed down (a.k.a. flash.err), whose value is another object with
            // the key of HealthcareProfessionalnamePasswordRequiredError
            req.session.flash = {
                err: HealthcareProfessionalnamePasswordRequiredError
            }

            res.redirect('/HealthcareProfessionalLogin/new');
            return;
        }

        // Try to find the HealthcareProfessional by their email address.
        // findOneByEmail() is a dynamic finder in that it searches the model by a particular attribute.
        // HealthcareProfessional.findOneByEmail(req.param('email')).done(function(err, HealthcareProfessional) {
        //console.log(req.param('Email'), req.param('Password')); //debug log
        //dynamic find methods allow us to use an attribute from the model
        HealthcareProfessional.findOneByEmail(req.param('Email'), function foundHealthcareProfessional(err, HealthcareProfessional) {
            if (err) return next(err);

            // If no HealthcareProfessional is found pass the error to flash and redirect to sign in page
            if (!HealthcareProfessional) {
                var noAccountError = [{
                    name: 'noAccount',
                    message: 'The email address ' + req.param('Email') + ' not found.'
                }]
                req.session.flash = {
                    err: noAccountError
                }
                res.redirect('/HealthcareProfessionalLogin/new');
                return;
            }

            // Compare password from the form params to the encrypted password of the HealthcareProfessional found.
            bcrypt.compare(req.param('Password'), HealthcareProfessional.Password, function(err, valid) {
                if (err) return next(err);

                // If the password from the form doesn't match the password from the database...
                if (!valid) {
                    var HealthcareProfessionalnamePasswordMismatchError = [{
                        name: 'HealthcareProfessionalnamePasswordMismatch',
                        message: 'Invalid HealthcareProfessionalname and password combination.'
                    }]
                    req.session.flash = {
                        err: HealthcareProfessionalnamePasswordMismatchError
                    }
                    res.redirect('/HealthcareProfessionalLogin/new');
                    return;
                }

                // Log HealthcareProfessional in
                req.session.authenticated = true;
                req.session.HealthcareProfessional = HealthcareProfessional;
                //res.json(HealthcareProfessional);
                // Change status to online
                HealthcareProfessional.online = true;
                res.redirect('/HealthcareProfessional/show/' + HealthcareProfessional.ProfessionalID);
                //HealthcareProfessional.save(function(err, HealthcareProfessional) {
                //    if (err) return next(err);
                //
                //    // Inform other sockets (e.g. connected sockets that are subscribed) that this HealthcareProfessional is now logged in
                //    //HealthcareProfessional.publishUpdate(HealthcareProfessional.id, {
                //    //    loggedIn: true,
                //    //    id: HealthcareProfessional.UserID,
                //    //    Name: HealthcareProfessional.FirstName,
                //    //    Lastname: HealthcareProfessional.LastName,
                //    //    action: ' has logged in.'
                //    //});
                //
                //    // If the HealthcareProfessional is also an admin redirect to the HealthcareProfessional list (e.g. /views/HealthcareProfessional/index.ejs)
                //    // This is used in conjunction with config/policies.js file
                //    //if (req.HealthcareProfessionalLogin.HealthcareProfessional.admin) {
                //    //    res.redirect('/HealthcareProfessional');
                //    //    return;
                //    //}
                //
                //    //Redirect to their profile page (e.g. /views/HealthcareProfessional/showAll.ejs)
                //    res.redirect('/HealthcareProfessional/show/' + HealthcareProfessional.UserID);
                //});
            });
        });
    },

    destroy: function(req, res, next) {
        req.session.destroy();
        res.redirect('HealthcareProfessionalLogin/new');
    }
};

