/**
 * Patient_CondtionController
 *
 * @description :: Server-side logic for managing Patient_condtions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'add': function(req,res,next){
        console.log(sails.config.myVariables.PatientID);
        var url = (req._parsedOriginalUrl.path).split('/');
        var Conditionid = url[url.length-1];
        Patient_Condition.create(
            {PatientID: sails.config.myVariables.PatientID,
            ConditionID: Conditionid,
            DateDiagnosed: "2016-09-04",
            State: "whatevs"}).exec(function (err, Patient_Condition){
            if (err) { return (err); }

          res.json(Patient_Condition);
        });
    }
};

