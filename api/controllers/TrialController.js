/**
 * TrialController
 *
 * @description :: Server-side logic for managing Trials
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var Async = require('async');
module.exports = {

    'new': function (req,res) {
        res.view();
    },

    'show': function (req,res,next) {
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];
        Trial.find({TrialID: id}).exec( function foundTrial(err,  Trial){
            if(err) {
                console.log("Trial not found");
                return next(err);
            }
            else if(!Trial){
                console.log("trial not found");
                return next();
            }else{
                // res.json(Trial);
                res.view({
                    Trial: Trial
                });
            }


        });
    },
    'create': function (req,res,next) {
        console.log("beginning trial creation");
        Trial.create(
            {
                ExternalTrialID: req.params.all().ExternalTrialID,
                OfficialName: req.params.all().OfficialName,
                AbbreviationName: req.params.all().AbbreviationName,
                KeyWords: req.params.all().KeyWords,
                BriefSummary: req.params.all().BriefSummary,
                ChiefInvestigator: req.params.all().ChiefInvestigator,
                TrialStatus: req.params.all().TrialStatus,
                DetailedDescription: req.params.all().DetailedDescription
                
            }, function TrialCreated(err, Trial){
            if(err){
                console.log(err);
                // req.session.flash ={
                //     err : err
                console.log("error when trying to create trial");
                return res.redirect('Trial/new');
                }
                
                console.log("trial was created successfully and added to the session");
                console.log("beginning trial condition creation");
                Trial_Condition.create(
                    {
                        TrialID: Trial.id,
                        ConditionID: req.params.all().ConditionID
                    },
                    function Trial_ConditionCreated(err, Trial_Condition ){
                        if(err) console.log(err);
                        if(!Trial_Condition){
                            console.log("Trial_condition was not created");
                            return res.redirect('Trial/new');
                        }
                        console.log("Trial_Condition was successfully created")
                    });
                console.log("Beginning creation of trial_clinicalCenter");
                    Trial_ClinicalCentre.create(
                        {
                            TrialID: Trial.id,
                            CentreID: req.params.all().CentreID,
                            StartDate: req.params.all().StartDate,
                            EndDate: req.params.all().EndDate,
                            PrincipalInvestigator: req.params.all().PrincipalInvestigator

                        },
                        function Trial_ClinicalCenterCreated(err, Trial_ClinicalCenter) {
                            if(err) return console.log(err);
                            if(!Trial_ClinicalCenter){
                                console.log("Trial_ClinicalCenter was not created!");
                                return;
                            }
                            console.log("Trial_ClinicalCenter was successfully created!");
                        }
                    );
                console.log("Beginning creation of ExclusionCriteria");
                ExclusionCriteria.create(
                    {
                        TrialID: Trial.id,
                        Gender: req.params.all().EGender,
                        MinimumAge: req.params.all().EMinimumAge,
                        MaximumAge: req.params.all().EMaximumAge,


                    }, function ExclusionCriteriaCreated(err, ExclusionCriteria) {
                        if(err) return console.log(err);
                        if(!ExclusionCriteria){
                            console.log("Exclusion criteria was not created!");
                            return;
                        }
                        console.log("Beginning creation of Trial_Exclusion");
                        Trial_Exclusion.create(
                            {
                                
                                TrialID: Trial.id,
                                ExclusionID: ExclusionCriteria.id,
                                Date: new Date(),
                                Time: new Date()


                            }, function Trial_ExclusionCreated(err, TrialExclusion) {
                                if(err) return console.log(err);
                                if(!TrialExclusion){
                                    console.log("Trial_Exclusion was not created!");
                                    return;
                                }
                                // req.session.ExclusionCriteria = ExclusionCrieteria;
                            }
                        );

                    }
                );
                console.log("Beginning creation of Inclusion criteria");
                InclusionCriteria.create(
                    {
                        TrialID: Trial.id,
                        Gender: req.params.all().IGender,
                        MinimumAge: req.params.all().IMinimumAge,
                        MaximumAge: req.params.all().IMaximumAge,

                    }, function InclusionCriteriaCreated(err, InclusionCriteria) {
                        if(err) return console.log(err);
                        if(!InclusionCriteria){
                            console.log("Exclusion criteria was not created!");
                            return;
                        }
                        console.log("Beginning creation of Trial_Inclusion");
                        Trial_Inclusion.create(
                            {
                                TrialID: Trial.id,
                                InclusionID: InclusionCriteria.id,
                                Date: new Date(),
                                Time: new Date()


                            }, function Trial_InclusionCreated(err, TrialInclusion) {
                                if(err) return console.log(err);
                                if(!TrialInclusion){
                                    console.log("Trial_Inclusion was not created!");
                                    return;
                                }
                                // res.redirect('Trial/show/'+ Trial.id)
                            }
                        );
                    }

                );
                Trial_Add.create(
                    {
                      TrialID: Trial.id,
                        Date: new Date(),
                        Time: new Date(),
                        ManagerID: req.session.Manager.ManagerID
                    },function Trial_AddCreated(err, Trial_Add) {
                        if(err) return console.log(err);
                        if(!Trial_Add){
                            console.log("Trial_Add was not created!");
                            return;
                        }
                        res.redirect('Trial/show/'+ Trial.id)
                    }
                );

            }); // callback END
    },
    'edit': function (req, res, next) {
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length - 1];

        Async.waterfall([
            function (callback) {
                Trial.find({TrialID: id}).exec(function foundTrial(err, Trial) {
                    //if no trial found
                    if (err) return next(err);
                    if (!Trial) return next();
                    res.Trial = Trial;
                    callback(err, res);
                });
            },
            function (res, callback){
                Trial_Condition.find({TrialID: id}).exec(function foundTrial(err, Trial_Condition) {
                    if (err)
                        throw new Error(err);
                    if (!Trial_Condition) return next();
                    //using the ConditionID in the Trial_Condition entry found above, find the respective Condition
                    Condition.find({ConditionID: Trial_Condition[0].ConditionID}).exec(function foundTrial(err, Condition) {
                        if (err)
                            throw new Error(err);
                        if (!Condition) return next();
                        //store the condition object in the session
                        res.Condition = Condition;
                        callback(err, res);
                    });
                });
            },
            function (res, callback) {
                Trial_Exclusion.find({TrialID: id}).exec(function foundTrial(err, ExclusionIDs) {
                    if (err) return next(err);
                    if (!ExclusionIDs) return next();
                    var ExclusionID = ExclusionIDs[ExclusionIDs.length - 1];
                    // console.log(ExclusionID);
                    ExclusionCriteria.find({ExclusionID: ExclusionID.ExclusionID}).exec(function foundTrial(err, ExclusionCriteria) {
                        if (err) return next(err);
                        if (!ExclusionCriteria) return next();
                        res.ExclusionCriteria = ExclusionCriteria;
                        // console.log(ExclusionCriteria);
                        callback(err,res);
                    });
                });
            },
            function (res, callback) {
                Trial_Inclusion.find({TrialID: id}).exec(function foundTrial(err, InclusionIDs) {
                    if (err) return next(err);
                    if (!InclusionIDs) return next();
                    var InclusionID = InclusionIDs[InclusionIDs.length - 1];
                    // console.log(ExclusionID);
                    InclusionCriteria.find({InclusionID: InclusionID.InclusionID}).exec(function foundTrial(err, InclusionCriteria) {
                        if (err) return next(err);
                        if (!InclusionCriteria) return next();
                        res.InclusionCriteria = InclusionCriteria;
                        // console.log(InclusionCriteria);
                        callback(err,res);
                    });
                });
            },function (res,callback) {
                //find the clinical center ids where the trial is taking place
                Trial_ClinicalCentre.find({TrialID: id}).exec(function foundTrial(err, ClinicalCentreID) {
                                if (err) return next(err);
                                if (!ClinicalCentreID) return next();
                    //find the clinical centre using the clinical centre id of the object found above
                    // console.log(ClinicalCentreID);
                    res.Trial_ClinicalCentre = ClinicalCentreID;
                    ClinicalCentre.find({CentreID: ClinicalCentreID[0].CentreID }).exec(function foundCentre(err, ClinicalCentre) {
                                if (err) return next(err);
                                if (!ClinicalCentre) return next();
                                // console.log(ClinicalCentre);
                                res.ClinicalCentre = ClinicalCentre;
                        callback(err, res);
                    });
                });
            },

        ], function (err, res) {


            res.view({
                Trial: res.Trial,
                Condition: res.Condition,
                ExclusionCriteria: res.ExclusionCriteria,
                InclusionCriteria: res.InclusionCriteria,
                ClinicalCentre: res.ClinicalCentre,
                Trial_ClinicalCentre: res.Trial_ClinicalCentre
            })
            
        })
    },

    'update': function (req,res,next) {
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length - 1];
        Async.waterfall([
            function (callback) {
                Trial.update({TrialID: id},
                    {
                        ExternalTrialID: req.body.ExternalTrialID,
                        OfficialName: req.body.OfficialName,
                        AbbreviationName: req.body.AbbreviationName,
                        KeyWords: req.body.KeyWords,
                        BriefSummary: req.body.BriefSummary,
                        ChiefInvestigator: req.body.ChiefInvestigator,
                        TrialStatus: req.body.TrialStatus,
                        DetailedDescription: req.body.DetailedDescription

                    }, function TrialUpdated(err, Trial) {
                        if (err) {
                            console.log(err);
                            console.log("error when trying to create trial");
                            return res.redirect('Trial/edit/' + id);
                        }
                            res.Trial = Trial;
                        callback(null,res);
                    });
            },
            //first parameter is the result of the previous function
            function (res, callback) {
                console.log("moving to redirection");
                callback(null, res);
            }

        ], function (err, res) {
            if(err){
                console.log(err);
            }
            console.log("it should redirect now");
            res.redirect('Trial/edit/'+ id);
        }); //Async waterfall end

    },

};

