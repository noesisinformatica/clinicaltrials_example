/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');

module.exports = {
    //this action will render/redirect to the form page
    'new': function(req, res ){
        //req.patientLogin.authenticated = true;
        //console.log(req.patientLogin);
        req.session.destroy();
        res.view();
    },
    create: function(req, res, next) {

        // Check for email and password in params sent via the form, if none
        // redirect the browser back to the sign-in form.
        if (!req.param('Email') || !req.param('Password')) {
            // return next({err: ["Password doesn't match password confirmation."]});

            var patientnamePasswordRequiredError = [{
                name: 'patientnamePasswordRequired',
                message: 'You must enter both a username and password.'
            }]

            // Remember that err is the object being passed down (a.k.a. flash.err), whose value is another object with
            // the key of patientnamePasswordRequiredError
            req.session.flash = {
                err: patientnamePasswordRequiredError
            }

            res.redirect('/patientLogin/new');
            return;
        }

        // Try to find the patient by their email address.
        // findOneByEmail() is a dynamic finder in that it searches the model by a particular attribute.
        // patient.findOneByEmail(req.param('email')).done(function(err, patient) {
        //console.log(req.param('Email'), req.param('Password')); //debug log
        //dynamic find methods allow us to use an attribute from the model
        patient.findOneByEmail(req.param('Email'), function foundpatient(err, patient) {
            if (err) return next(err);

            // If no patient is found pass the error to flash and redirect to sign in page
            if (!patient) {
                var noAccountError = [{
                    name: 'noAccount',
                    message: 'The email address ' + req.param('Email') + ' not found.'
                }]
                req.session.flash = {
                    err: noAccountError
                }
                res.redirect('/patientLogin/new');
                return;
            }

            // Compare password from the form params to the encrypted password of the patient found.
            bcrypt.compare(req.param('Password'), patient.Password, function(err, valid) {
                if (err) return next(err);

                // If the password from the form doesn't match the password from the database...
                if (!valid) {
                    var patientnamePasswordMismatchError = [{
                        name: 'patientnamePasswordMismatch',
                        message: 'Invalid patientname and password combination.'
                    }]
                    req.session.flash = {
                        err: patientnamePasswordMismatchError
                    }
                    res.redirect('/patientLogin/new');
                    return;
                }

                // Log patient in
                req.session.authenticated = true;
                req.session.patient = patient;
                //res.json(patient);
                // Change status to online
                patient.online = true;
                sails.config.myVariables.PatientID = patient.PatientID;
                res.redirect('/patient/show/' + patient.PatientID);
                //patient.save(function(err, patient) {
                //    if (err) return next(err);
                //
                //    // Inform other sockets (e.g. connected sockets that are subscribed) that this patient is now logged in
                //    //patient.publishUpdate(patient.id, {
                //    //    loggedIn: true,
                //    //    id: patient.PatientID,
                //    //    Name: patient.FirstName,
                //    //    Lastname: patient.LastName,
                //    //    action: ' has logged in.'
                //    //});
                //
                //    // If the patient is also an admin redirect to the patient list (e.g. /views/patient/index.ejs)
                //    // This is used in conjunction with config/policies.js file
                //    //if (req.patientLogin.patient.admin) {
                //    //    res.redirect('/patient');
                //    //    return;
                //    //}
                //
                //    //Redirect to their profile page (e.g. /views/patient/showAll.ejs)
                //    res.redirect('/patient/show/' + patient.PatientID);
                //});
            });
        });
    },

    destroy: function(req, res, next) {
            req.session.destroy();
        res.redirect('patientLogin/new');
    }
};

