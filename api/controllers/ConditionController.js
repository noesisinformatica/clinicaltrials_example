/**
 * ConditonController
 *
 * @description :: Server-side logic for managing Conditons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	'showAll' : function(req,res,next){
        Condition.find(function foundConditions(err, Conditions){
            if(err) return next(err);
            res.view({Conditions : Conditions});
            //res.json(Conditions);
        });

}
}

