/**
 * PatientController
 *
 * @description :: Server-side logic for managing patients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'new': function (req,res) {
        res.locals.flash = _.clone(req.session.flash);
        res.view();
        req.session.flash = {};

    },
    create: function (req,res,next) {
        patient.create(req.params.all(), function patientCreated(err, patient){
            if(err){
                console.log(err);
                req.session.flash ={
                    err : err
                }
                return res.redirect('patient/new');
            }
            //log the patient in the patientLogin
            req.session.authenticated = true;
            req.session.patient = patient;
            //var pa = (res.json(patient));
            res.redirect('/patient/show/'+ patient.id);
            //res.json(patient);
            req.session.flash = {};
        });
        
    },
    'show': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];
        //console.log(id);
        patient.find({PatientID: id}).exec( function foundPatient(err,  patient){
            if(err) return next(err);
            if(!patient) return next();
            //res.json(patient);
            res.view({
                patient: patient
            });

        });
    },
    'edit': function(req,res,next){
        res.locals.flash = _.clone(req.session.flash);

        // Find the user from the id passed in via params
            patient.find({PatientID: req.param('id')}).limit(1).exec (function foundUser(err, patient) {
                if (err) return next(err);
                if (!patient) return next('User doesn\'t exist.');
                //if a manager is logged in there will be no patient details in the session
                //this ensures patient to be edited is in the session to be used by the
                //update method ,the if statement is there in case a patient wants to do a change instead of a manager
                //if(!req.session.patient) req.session.patient = patient;
                //redirects to the edit page and sets patient info in the session to equal the patient found by the
                //query above
                res.view({
                    patient: patient
                });
                //res.json(req.session.patient);
            });
        },

    'update': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];

        console.log(id);
        patient.update(id, req.params.all(), function patientUpdated(err){
            if(err){
                return res.redirect('/patient/edit/'+ id);
            }

            res.redirect('/patient/show/'+ id);
            //res.json(req.session.patient);
        });


    },

    'delete': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];
        console.log(id);
        patient.find({PatientID: id}).limit(1).exec( function foundPatient(err, patient) {
            if (err) return next(err);

            if (!patient) return next('Patient doesn\'t exist.');
        });
            patient.destroy(id, function patientDestroyed(err) {
                if (err) return next(err);

                // Inform other sockets (e.g. connected sockets that are subscribed) that this user is now logged in
                //User.publishUpdate(user.id, {
                //    name: user.name,
                //    action: ' has been destroyed.'
                //});

                // Let other sockets know that the user instance was destroyed.
                //User.publishDestroy(user.id);
                res.redirect('/patient');
            });


    }


};
