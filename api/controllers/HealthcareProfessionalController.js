/**
 * HealthcareProfessionalController
 *
 * @description :: Server-side logic for managing Healthcareprofessionals
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'new': function (req,res) {
        res.locals.flash = _.clone(req.session.flash);
        res.view();
        req.session.flash = {};

    },
    create: function (req,res,next) {
        HealthcareProfessional.create(req.params.all(), function HealthcareProfessionalCreated(err, HealthcareProfessional){
            if(err){
                console.log(err);
                req.session.flash ={
                    err : err
                }
                return res.redirect('HealthcareProfessional/new');
            }
            //log the HealthcareProfessional in the patientLogin
            req.session.authenticated = true;
            req.session.HealthcareProfessional = HealthcareProfessional;
            //var pa = (res.json(HealthcareProfessional));
            res.redirect('/HealthcareProfessional/show/'+ HealthcareProfessional.id);
            //res.json(HealthcareProfessional);
            req.session.flash = {};
        });

    },
    'show': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];
        console.log(id);
        HealthcareProfessional.find({ProfessionalID: req.param('id')}).exec( function foundHealthcareProfessional(err,  HealthcareProfessional){
            if(err) return next(err);
            if(!HealthcareProfessional) return next();
            res.json(HealthcareProfessional);
            //res.view({
            //    HealthcareProfessional: HealthcareProfessional
            //});

        });
    },
    'edit': function(req,res,next){
        res.locals.flash = _.clone(req.session.flash);
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];

        // Find the user from the id passed in via params
        HealthcareProfessional.find({ProfessionalID: id }).limit(1).exec (function foundUser(err, HealthcareProfessional) {
            if (err) return next(err);
            if (!HealthcareProfessional) return next('User doesn\'t exist.');
            //if a manager is logged in there will be no patient details in the session
            //this ensures patient to be edited is in the session to be used by the
            //update method ,the if statement is there in case a patient wants to do a change instead of a manager
            //if(!req.session.patient) req.session.patient = patient;
            //redirects to the edit page and sets patient info in the session to equal the patient found by the
            //query above
            res.view({
                HealthcareProfessional: HealthcareProfessional
            });
            //res.json(req.session.patient);
        });
    },
    'update': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];

        console.log(id);
        HealthcareProfessional.update(id, req.params.all(), function patientUpdated(err){
            if(err){
                console.log('error when updating HP');
                console.log(err);
                return res.redirect('/HealthcareProfessional/edit/'+ id);
            }

            res.redirect('/HealthcareProfessional/show/'+ id);
            //res.json(req.session.patient);
        });

    },
    'delete': function(req, res, next){
        var url = (req._parsedOriginalUrl.path).split('/');
        var id = url[url.length-1];
        //console.log(id);
        HealthcareProfessional.find({ProfessionalID: id}).limit(1).exec( function foundPatient(err, HealthcareProfessional) {
            if (err) return next(err);

            if (!HealthcareProfessional) return next('HealthcareProfessional doesn\'t exist.');
        });
        HealthcareProfessional.destroy(id, function HealthcareProfessionalDestroyed(err) {
            if (err) return next(err);

            // Inform other sockets (e.g. connected sockets that are subscribed) that this user is now logged in
            //User.publishUpdate(user.id, {
            //    name: user.name,
            //    action: ' has been destroyed.'
            //});

            // Let other sockets know that the user instance was destroyed.
            //User.publishDestroy(user.id);
            res.redirect('/HealthcareProfessional');
        });
    }

};

