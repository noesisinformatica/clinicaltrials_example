/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');

module.exports = {
    //this action will render/redirect to the form page
    'new': function(req, res ){
        //req.ManagerLogin.authenticated = true;
        //console.log(req.ManagerLogin);
        req.session.destroy();
        res.view();
    },
    create: function(req, res, next) {

        // Check for email and password in params sent via the form, if none
        // redirect the browser back to the sign-in form.
        if (!req.param('Email') || !req.param('Password')) {
            // return next({err: ["Password doesn't match password confirmation."]});

            var ManagernamePasswordRequiredError = [{
                name: 'ManagernamePasswordRequired',
                message: 'You must enter both a username and password.'
            }]

            // Remember that err is the object being passed down (a.k.a. flash.err), whose value is another object with
            // the key of ManagernamePasswordRequiredError
            req.session.flash = {
                err: ManagernamePasswordRequiredError
            }

            res.redirect('/ManagerLogin/new');
            return;
        }

        // Try to find the Manager by their email address.
        // findOneByEmail() is a dynamic finder in that it searches the model by a particular attribute.
        // Manager.findOneByEmail(req.param('email')).done(function(err, Manager) {
        //console.log(req.param('Email'), req.param('Password')); //debug log
        //dynamic find methods allow us to use an attribute from the model
        Manager.findOneByEmail(req.param('Email'), function foundManager(err, Manager) {
            if (err) return next(err);

            // If no Manager is found pass the error to flash and redirect to sign in page
            if (!Manager) {
                var noAccountError = [{
                    name: 'noAccount',
                    message: 'The email address ' + req.param('Email') + ' not found.'
                }]
                req.session.flash = {
                    err: noAccountError
                }
                res.redirect('/ManagerLogin/new');
                return;
            }

            // Compare password from the form params to the encrypted password of the Manager found.
            bcrypt.compare(req.param('Password'), Manager.Password, function(err, valid) {
                if (err) return next(err);

                // If the password from the form doesn't match the password from the database...
                if (!valid) {
                    var ManagernamePasswordMismatchError = [{
                        name: 'ManagernamePasswordMismatch',
                        message: 'Invalid Managername and password combination.'
                    }]
                    req.session.flash = {
                        err: ManagernamePasswordMismatchError
                    }
                    res.redirect('/ManagerLogin/new');
                    return;
                }

                // Log Manager in
                req.session.authenticated = true;
                req.session.Manager = Manager;
                //res.json(Manager);
                // Change status to online
                Manager.online = true;
                res.redirect('/Manager/show/' + Manager.ManagerID);
                //Manager.save(function(err, Manager) {
                //    if (err) return next(err);
                //
                //    // Inform other sockets (e.g. connected sockets that are subscribed) that this Manager is now logged in
                //    //Manager.publishUpdate(Manager.id, {
                //    //    loggedIn: true,
                //    //    id: Manager.UserID,
                //    //    Name: Manager.FirstName,
                //    //    Lastname: Manager.LastName,
                //    //    action: ' has logged in.'
                //    //});
                //
                //    // If the Manager is also an admin redirect to the Manager list (e.g. /views/Manager/index.ejs)
                //    // This is used in conjunction with config/policies.js file
                //    //if (req.ManagerLogin.Manager.admin) {
                //    //    res.redirect('/Manager');
                //    //    return;
                //    //}
                //
                //    //Redirect to their profile page (e.g. /views/Manager/showAll.ejs)
                //    res.redirect('/Manager/show/' + Manager.UserID);
                //});
            });
        });
    },

    destroy: function(req, res, next) {
        req.session.destroy();
        res.redirect('ManagerLogin/new');
    }
};

