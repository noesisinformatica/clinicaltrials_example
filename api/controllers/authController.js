var passport = require('passport');

module.exports = {

    _config: {
        //overriding the default blueprints configurations for this controller
        actions: true,
        shortcuts: true,
        rest: true
    },
    login: function(req, res) {

        passport.authenticate('local', function(err, patient, info) {
            if ((err) || (!patient)) {
                return res.send({
                    message: info.message,
                    patient: patient
                });
            }
            req.logIn(patient, function(err) {
                if (err) res.send(err);
                return res.send({
                    message: info.message,
                    patient: patient
                });
            });

        })(req, res);
    },
    logout: function(req, res) {
        req.logout();
        res.redirect('/');
    }
};