module.exports = function(req, res, next) {

    // User is allowed, proceed to the next policy,
    // or if this is the last policy, the controller
    var url = (req.url).split('/');
    var id = url[url.length-1];
    //console.log(id);
    //console.log(req.session.patient.UserID);
    //console.log(req.session.patient.UserID == id);
    if (req.session.authenticated === true && ( req.session.Manager || req.session.HealthcareProfessional.ProfessionalID == id)) {
        return next();
    }else{
        var requireLoginError = [{name: 'requireLogin', message: 'You must be signed in'}];
        req.session.flash = {
            err: requireLoginError
        }
        res.redirect('/HealthcareProfessional/new');
        return;
    }
};