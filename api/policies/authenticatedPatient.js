/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
module.exports = function(req, res, next) {

  // User is allowed, proceed to the next policy, 
  // or if this is the last policy, the controller
    var url = (req.url).split('/');
    var id = url[url.length-1];
    //console.log(id);
    //console.log(req.session.patient.PatientID);
    //console.log(req.session.patient.PatientID == id);
  if (req.session.authenticated === true && (req.session.patient.PatientID == id)) {
    return next();
  }else{
    var requireLoginError = [{name: 'requireLogin', message: 'You must be signed in'}];
    req.session.flash = {
      err: requireLoginError
    }
    res.redirect('/patientLogin/new');
    return;
  }
};
